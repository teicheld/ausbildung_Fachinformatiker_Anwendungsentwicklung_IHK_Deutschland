Die Technik besteht aus fünf Schritten:

    die Aufgabe schriftlich formulieren
    den Kurzzeitwecker auf 25 Minuten stellen
    die Aufgabe bearbeiten, bis der Wecker klingelt; mit einem X markieren
    kurze Pause machen (5 Minuten)
    nach jeweils vier Zeitblöcken eine längere Pause machen (15–20 Minuten)
